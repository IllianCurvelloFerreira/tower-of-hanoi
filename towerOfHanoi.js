let pecaselecionada;
const torre1 = document.getElementById("first-pin");
const torre2 = document.getElementById("second-pin");
const torre3 = document.getElementById("third-pin");
const buttonreset = document.getElementById("butreset")
torre1.addEventListener("click", (e) => seletor(e))
torre2.addEventListener("click", (e) => seletor(e))
torre3.addEventListener("click", (e) => seletor(e))
buttonreset.addEventListener("click", resetar)
function seletor(e) {
    let alvo = e.currentTarget;
    if (pecaselecionada) {
        soltapeca(alvo);
    } else {
        pegapeca(alvo);
    }
}
function pegapeca(coluna) {
    pecaselecionada = coluna.lastElementChild;
}
function soltapeca(coluna) {
    if (coluna.lastElementChild == null) {
        coluna.appendChild(pecaselecionada)
        pecaselecionada = null
    } else if (coluna.lastElementChild.clientWidth > pecaselecionada.clientWidth) {
        coluna.appendChild(pecaselecionada)
        pecaselecionada = null
        verifyEndGame()
    } else {
        pecaselecionada = null
    }
}
let botao = document.createElement("button")
let res1 = document.getElementById("resultado")
function verifyEndGame() {
    if (torre3.childElementCount == 4) {
        res1.innerHTML = ("Parabéns, você concluiu o desafio da Torre de Hanoi!")
        buttonreset.appendChild(botao)
        botao.innerHTML = (" RESET ");
    }
}
function resetar() {
    for (i = 1; i <= 4; i++) {
        let varReseta = torre3.firstElementChild;
        torre1.appendChild(varReseta);
    }
    res1.innerHTML = ("Bom Jogo e Divirta-se!");
    document.getElementById("butreset").style.display = "none";
}